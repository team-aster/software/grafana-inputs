# Inputs panel

A panel that can display inputs.

## Build

1. Install dependencies

   ```bash
   yarn install
   ```

2. Build plugin in development mode and run inside Grafana using Docker

   ```bash
   # Start watching for changes
   yarn dev

   # Run Grafana inside a docker container in a separate session
   docker-compose up
   ```

3. Build plugin in production mode

   ```bash
   yarn build
   ```

4. Run e2e tests

   ```bash
   yarn e2e
   ```
