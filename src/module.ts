import {PanelPlugin} from '@grafana/data';
import {InputsPanelOptions} from './options';
import {InputsPanel} from './InputsPanel';
import {getTemplateSrv} from '@grafana/runtime';

export const plugin = new PanelPlugin<InputsPanelOptions>(InputsPanel).setPanelOptions(builder => {
    const variables = getTemplateSrv()
        .getVariables()
        .map(variable => {
            return {
                label: variable.label || variable.name,
                value: variable.name,
            };
        });
    return builder
        .addSelect({
            path: 'valueVariable',
            name: 'Value variable',
            description: 'The variable to store the input to.',
            settings: {
                allowCustomValue: false,
                options: variables,
            },
        })
        .addFieldNamePicker({
            path: 'typeQueryField',
            name: 'Type Field',
            description: 'The name of the field that will provide the type of each input.',
            defaultValue: '__type',
        })
        .addFieldNamePicker({
            path: 'idQueryField',
            name: 'Id Field',
            description: 'The name of the field that will provide an unique Id of each input.',
        })
        .addFieldNamePicker({
            path: 'nameQueryField',
            name: 'Name Field',
            description: 'The name of the field that will provide the name of each input.',
        })
        .addBooleanSwitch({
            path: 'saveAsJson',
            name: 'Save value as JSON',
            description: 'Whether to save the value as a Json object including the name.',
            defaultValue: false,
            showIf: options => options.nameQueryField !== undefined,
        })
        .addFieldNamePicker({
            path: 'docQueryField',
            name: 'Description Field',
            description: 'The name of the field that will provide a description of each input.',
        })
        .addFieldNamePicker({
            path: 'defaultsQueryField',
            name: 'Defaults Field',
            description: 'The name of the field that will provide the default value of each input.',
        })
        .addBooleanSwitch({
            path: 'showUnits',
            name: 'Show Units',
            description: 'Whether to show units, if they are included in the type.',
            defaultValue: true,
        })
        .addSelect({
            path: 'clearVariable',
            name: 'Clear Variable',
            description: 'The name of a variable that will be cleared when the value changes.',
            settings: {
                allowCustomValue: false,
                options: variables,
            },
        });
});
