/**
 * The panel options for the Inputs panel.
 */
export interface InputsPanelOptions {
    /**
     * The name of the variable that will be replaced by the value entered into the input.
     */
    valueVariable: string;

    /**
     * Whether or not to save the input as a JSON string.
     */
    saveAsJson: boolean;

    /**
     * An optional variable to clear when the value of the input changes.
     */
    clearVariable?: string;

    /**
     * The name of the type query field. This will provide information about the type of each input.
     */
    typeQueryField: string;

    /**
     * The name of the id query field.
     * This provides an id that can be used to differentiate inputs with the same name and type.
     */
    idQueryField?: string;

    /**
     * The name of the query field which provides the name for each input.
     */
    nameQueryField?: string;

    /**
     * The name of the query field which provides a description or help message of each input.
     */
    docQueryField?: string;

    /**
     * The name of the query field which provides the default value for each input.
     */
    defaultsQueryField?: string;

    /**
     * Whether to show units if present in the type info.
     */
    showUnits: boolean;
}
