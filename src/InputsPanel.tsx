import React, {useState} from 'react';
import {Field, InterpolateFunction, PanelData, PanelProps, UrlQueryMap} from '@grafana/data';
import {InputsPanelOptions} from 'options';
import {css, cx} from '@emotion/css'
import {useStyles, Label, Alert, InlineField, InlineFieldRow, Switch, Input, Select} from '@grafana/ui';
import {locationService} from '@grafana/runtime';

/**
 * The properties of the Input panel.
 */
interface Props extends PanelProps<InputsPanelOptions> {
}

/**
 * A value of an enum type.
 */
interface EnumValue {
    /**
     * The display name of the value.
     */
    name?: string;

    /**
     * The description of the value.
     */
    description?: string;
}

/**
 * Supported type names.
 */
declare type typeName = 'bool' | 'int' | 'float' | 'string' | 'enum';

/**
 * A description of a type.
 */
interface TypeInfo {
    /**
     * The name of the type.
     */
    name?: typeName;

    /**
     * The unit of the type.
     */
    unit?: string;

    /**
     * The value of the enum type if the type is an enum.
     */
    values?: EnumValue[];
}

/**
 * An input that should be displayed in the panel.
 */
interface InputElement {
    /**
     * The type of this input.
     */
    typeInfo: TypeInfo | null;

    /**
     * An optional key identifying this input.
     */
    key?: string;

    /**
     * The display name of this input.
     */
    name?: string;

    /**
     * A description of this input.
     */
    description?: string;

    /**
     * The default value of this input.
     */
    defaultValue?: any;
}

/**
 * Find a field with a given name in the panel data.
 *
 * @param data The panel data.
 * @param name The name of the field to find.
 * @return The field with the given name or null.
 */
function _findField(data: PanelData, name: string): Field | null {
    for (const series of data.series) {
        for (const field of series.fields) {
            if (field.name === name) {
                return field;
            }
        }
    }
    return null;
}

/**
 * Get the current values for a Grafana dashboard variable.
 *
 * @param variableResolver The function that will be used to retrieve the variable content.
 * @param name The name of the variable
 */
function _getValuesForVariable(variableResolver: InterpolateFunction, name: string): string[] {
    const values: string[] = [];
    // Instead of interpolating the string, we collect the values in an array.
    variableResolver(`$${name}`, {}, (value: string | string[]) => {
        if (Array.isArray(value)) {
            values.push(...value);
        } else {
            values.push(value);
        }
        return ''; // We don't really care about the string here.
    });
    return values;
}

/**
 * A panel that displays inputs.
 *
 * @param options The settings of the panel.
 * @param data The data to display in the panel.
 * @param width The width of the panel in pixels.
 * @param height The height of the panel in pixels.
 * @param replaceVariables A function to replace template variables.
 */
export const InputsPanel: React.FC<Props> = (
    {options, data, width, height, replaceVariables}
) => {
    // [state] holds the values of the inputs that are displayed.
    const [state, setState] = useState(() => {
        let initialState: Record<string | number, any> = {};
        let currentValues = _getValuesForVariable(replaceVariables, options.valueVariable);
        if (currentValues.length) {
            if (options.saveAsJson) {
                try {
                    initialState = JSON.parse(currentValues[0]);
                } catch (_) {
                }
            } else {
                currentValues.forEach((value, index) => {
                    initialState[index] = value;
                });
            }
        }
        return initialState;
    });

    /**
     * Handle the change of an input.
     *
     * @param key The state key of the input that changed.
     * @param value The new value of the input.
     */
    function _onValueChange(key: string | number, value: any) {
        let changed = value !== state[key];
        state[key] = value;
        setState({...state});
        _updateStateVariable(changed);
    }

    /**
     * Update the input state variable in the dashboard.
     *
     * @param hasChanged Whether or not any of the values in the state variable have changed.
     */
    function _updateStateVariable(hasChanged = true) {
        let stateValue;
        const values = Object.values(state);
        if (options.saveAsJson) {
            stateValue = JSON.stringify(state);
        } else if (values.length > 1) {
            stateValue = values;
        } else {
            stateValue = values[0];
        }
        let query: UrlQueryMap = {
            [`var-${options.valueVariable}`]: stateValue,
        };
        if (hasChanged && options.clearVariable) {
            query[`var-${options.clearVariable}`] = null;
        }
        locationService.partial(query, true);
    }

    const styles = useStyles(inputPanelStyles);
    const typeField = _findField(data, options.typeQueryField);
    const nameField = options.nameQueryField ? _findField(data, options.nameQueryField) : null;
    if (!typeField) {
        return <Alert title={`Unable to find type field "${options.typeQueryField}"`}/>;
    }
    const idField = options.idQueryField ? _findField(data, options.idQueryField) : null;
    const descriptionField = options.docQueryField ? _findField(data, options.docQueryField) : null;
    const defaultValueField = options.defaultsQueryField ? _findField(data, options.defaultsQueryField) : null;
    let inputs: InputElement[] = [];
    const panelId = `${data.request?.dashboardId}-${data.request?.panelId}`;
    for (let i = 0; i < (typeField?.values.length || 0); i++) {
        const id = idField?.values.get(i) || idField?.values.get(0);
        let typeInfo = typeField!.values.get(i);
        if (typeof typeInfo === 'string') {
            typeInfo = JSON.parse(typeInfo);
        }
        if (typeof typeInfo === 'object') {
            if (typeof typeInfo.dependsOn === 'string' && typeof typeInfo.types === 'object') {
                const dependentVariableName = typeInfo.dependsOn;
                let dependentVariableValue;
                if (options.saveAsJson) {
                    try {
                        dependentVariableValue = state[dependentVariableName];
                    } catch (_) {
                    }
                } else {
                    dependentVariableValue = _getValuesForVariable(replaceVariables, dependentVariableName)[0];
                }
                if (!dependentVariableValue) {
                    typeInfo = null;
                } else {
                    const actualType: any = typeInfo.types[dependentVariableValue];
                    if (typeof actualType !== 'object') {
                        typeInfo = null;
                    } else {
                        typeInfo = actualType;
                    }
                }
            }
        } else if (typeInfo != null) {
            return <Alert title={`Invalid typ field value: "${typeInfo}"  (${typeof typeInfo})`}/>;
        }
        inputs.push({
            typeInfo: typeInfo,
            key: id ? `${panelId}-${id}-${i}` : undefined,
            name: nameField?.values.get(i),
            description: descriptionField?.values.get(i),
            defaultValue: defaultValueField?.values.get(i),
        });
    }
    let haveStateUpdate = false;
    Object.keys(state).forEach((key, index) => {
        const isValidKey = options.saveAsJson ? inputs.some((input) => input.name === key) : index < inputs.length;
        if (!isValidKey) {
            delete state[key];
            haveStateUpdate = true
        }
    });
    if (haveStateUpdate) {
        locationService.partial({
                [`var-${options.valueVariable}`]: null,
            },
            true,
        );
    }

    return (
        <div
            className={cx(
                styles.wrapper,
                css`
                  width: ${width}px;
                  height: ${height}px;
                `
            )}>
            <InlineFieldRow>
                {inputs.map((input, index) => {
                    const stateKey = input.name || index;
                    const doStateUpdate = !(stateKey in state)
                    const defaultValue = stateKey in state ? state[stateKey] : input.defaultValue;
                    let inputElement;
                    const typeName = input.typeInfo?.name;
                    if (typeName === 'bool') {
                        inputElement = (
                            <Switch
                                key={input.key}
                                // @ts-ignore
                                label={null}
                                tooltip={input.description}
                                checked={defaultValue === 'true'}
                                onChange={event => _onValueChange(stateKey, event.currentTarget.checked)}
                            />
                        );
                    } else if (typeName === 'enum') {
                        inputElement = (
                            <Select
                                key={input.key}
                                options={input.typeInfo?.values?.map(value => {
                                    return {key: value.name, label: value.name, description: value.description};
                                })}
                                defaultValue={{key: defaultValue, label: defaultValue}}
                                menuPosition={'fixed'}
                                menuShouldPortal={true}
                                onChange={value => _onValueChange(stateKey, value.key)}
                            />
                        );
                    } else {
                        inputElement = (
                            <Input
                                key={input.key}
                                style={{width: 'auto'}}
                                defaultValue={defaultValue}
                                type={typeName === 'int' || typeName === 'float' ? 'number' : 'text'}
                                onChange={event => _onValueChange(stateKey, event.currentTarget.value)}
                            />
                        );
                    }
                    if (doStateUpdate) {
                        state[stateKey] = defaultValue;
                        _updateStateVariable(true);
                    }
                    let field;
                    if (input.name) {
                        field = (
                            <InlineField
                                style={{alignItems: 'center'}}
                                label={input.name}
                                defaultValue={defaultValue}
                                tooltip={input.description}>
                                {inputElement}
                            </InlineField>
                        );
                    } else {
                        field = <div style={{display: 'flex'}}>{inputElement}</div>;
                    }
                    if (!input.typeInfo?.unit || !options.showUnits) {
                        return field;
                    }
                    return (
                        <div className={styles.unitWrapper} key={`${input.key}-unit`}>
                            {field}
                            <Label style={{paddingLeft: '4px'}}>{input.typeInfo.unit}</Label>
                        </div>
                    );
                })}
            </InlineFieldRow>
        </div>
    );
};

/**
 * The styles used in the inputs panel.
 */
const inputPanelStyles = () => {
    return {
        wrapper: css`
          position: relative;
          display: flex;
          flex-direction: row;
          justify-content: center;
          align-items: center;
        `,
        unitWrapper: css`
          display: flex;
          flex-direction: row;
          justify-content: center;
          align-items: center;
        `,
        label: css`
          display: flex;
          flex-direction: row;
        `,
        input: css`
          margin-left: 8px;
        `,
    };
};
